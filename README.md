# Josquin Composer Attribution
Vishesh Gupta, Jacqueline Speiser 

## Overview
This project contains data pertaining to music we know is Josquin's, music 
we know isn't Josquin's, and music we're not sure is Josquin or not. 

The goal is to try and classify to the best of our understanding what is and
what isn't Josquin's music. 

## Documentation 
There are a series of scripts in the scripts/ folder, and at first glance it may not be clear how to best use them all. Let's go through a standard workflow.

First, a bunch of feature extraction scripts are located in scripts/. Each of these files outputs a two column feature list - a label, and then a value. The labels should be consistent and unique tags for each unique feature that is being extracted from the file. It's important the labeling scheme be unique, so that the aggregation script can do its magic.

To generate the fully padded/cleaned feature matrix, first list all the feature scripts you would like to run in the file featurescripts. Actually, you can use any plain text file with one script per line, but for the simplicity of writing out the commands, I'm going to assume you've put them all in featurescripts. 

Then, run aggregatefeatures.bash. For a new configuration of script files, I first recommend generating and storing the masterfeature list like so:

scripts/aggregatefeatures.bash -f featurescripts path/to/data [path/to/more/data...] > masterfeatures

This will take a minute to run. The upside is you save yourself a lot of wait time to run the next part, by passing in a -m flag

scripts/aggregatefeatures.bash -m masterfeatures featurescripts path/to/data [path/to/more/data] | tee features/somefeaturefile.csv

Alternatively, if you *really* don't want to generate the intermediate features and store them, you can run:

scripts/aggregatefeatures.bash featurescripts path/to/data [path/to/more/data] | tee features/somefeaturefile.csv

The .csv is relatively important to have it be imported by Mathematica properly.

Once you have the features, you can label them. Note that while the other steps would work with any inputs (so not even the data we specify), the labelfeatures.bash script is dependent on the naming scheme present in the data. If the name of the file has "Joa" in it, then we know it's a true Josquin piece. If it has a Job in it, then we don't know and correspondingly output a "?". For testing purposes, you'd probably want to use either all ones or all zeros, and then the "error" would tell you what percentage of the pieces were Josquin's or not. 


## Acknowledgements
This project is being sponsored by the Stanford CCARH center.

