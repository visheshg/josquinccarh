#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* File: durationhistogram.m 
  Author: Vishesh Gupta
  Created: 10 December 2013

  This file takes in the name of a cleaned notearray file and outputs transition
  matrices depending on the options that are passed in. 

  scripts/markov.m -r file.dat - gives a markov model of the rhythm chain.
  The alphabet data for that chain is a fixed rhythm alphabet of 
  {whole, dotted half, half, dotted quarter, quarter, eighth, sixteenth}, which
  has been found to be sufficient for renaissance music (we rarely see dotted 
  eigths.)

  scripts/markov.m -p file.dat - gives a markov model of the pitch chain for 
  each voice. This time, the alphabet is generated on the fly from the actual 
  pitches used in the piece. Any one piece uses ~10-15 pitches per voice 
  (depending on the length), so this thing will output about 100 features
  per file. (O(n^2) in the number of pitches) 

  scripts/markov.m -b file.dat - gives a markov model using the alphabet
  of BOTH rhythm and pitch (so an ordered pair of all pitch/rhythm) combos
  in the piece. 
*)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 2, 
  Print["Usage: markov.m -[rpb] datafile"]; Exit[]];


(* =============================== FUNCTIONS =============================== *)

ExpandedMinRhythm::usage="ExpandedMinRhythm[val, alphabet] takes a decimal value
  and a rhythm alphabet where each entry is of the form {'Q', 0.25} (so a string
  name and a decimal value for what it's duration is) and calculates the 
  minimum representation of that value. It's like finding the minumum coin
  representation of a change value.";
ExpandedMinRhythm[val_, alphabet_] := Module[{descr, temp, num, i},
  descr = {};
  temp = val;
  For[i = 1, i <= Length[alphabet] && temp > 0, i++,
    num = Floor[temp/alphabet[[i, 2]] ];
    temp = Mod[temp, alphabet[[i, 2]] ];

    (* correction for non-terminating deicmal values like 1/3 *)
    If[StringMatchQ[ToString[temp], RegularExpression["0\\.33.*"] ],
      temp = 0.333;
    ];
    If[StringMatchQ[ToString[temp], RegularExpression["0\\.66.*"] ],
      temp = 0.666;
    ];
    
    If[num > 0, AppendTo[descr, Table[alphabet[[i, 1]], {num}] ]];
  ];
  (* We are going to ignore any other strange occurences of notes
  like 0.167 which are happening and just round them to the nearest 
  rhythm value. As it is, E never happens, so tE = E is a fine approximation.*)
  (* If[temp - 0.001 > 0, AppendTo[descr, {"R", temp}]]; *)
  Return[descr // Flatten]
];

ExtractRhythm[notes_, alphabet_, col_] := 
  ExpandedMinRhythm[notes[[#1[[col+3]] ]][[5]] - #1[[5]], alphabet] & /@ 
  Select[notes, 
    #[[col]] >= 0 && #[[col+2]] > 0 && #[[1]] == #[[col+1]] &] // Flatten;

FirstOrderMarkov[alphabet_, list_] := 
  Module[{prev, i, transitions, abindexes},
   transitions = 
    ConstantArray[0, {Length[alphabet], Length[alphabet]}];
   For[i = 1, i <= Length[alphabet], i++,
    abindexes[alphabet[[i]]] = i
    ];
   prev = list[[1]];
   For[i = 2, i <= Length[list], i++,
    transitions[[ abindexes[prev], abindexes[list[[i]] ] ]] += 1;
    prev = list[[i]];
    ];
   Return[Normalize[#, Total] & /@ transitions];
   ];

MarkovFeatureName[alphabet_, transitions_] := 
  Flatten[MapIndexed[{StringJoin[
       Riffle[alphabet[[#1]] & /@ #2, "-"] ], #1} &, 
    transitions, {2}], 1];

MarkovWrapper[alphabet_, events_] := If[events == {}, {}, 
  MarkovFeatureName[alphabet, FirstOrderMarkov[alphabet, events]]
];

FeatureName::usage="FeatureName[hist, name, voice] takes a Nx2 matrix 
that is intended to be a histogram, and prepends some string label to the 
first column in the order 'name-voice-[[1]]', where [[1]] is the first 
part of every pair in the histogram.";
FeatureName[hist_, name_String, voice_String] := 
  {name <>"-" <> voice <> "-" <> ToString[#1[[1]] ], #1[[2]]} & /@ hist;

DumpToConsole::usage="Writes out a matrix comma separated to stdout. 
Everything is first converted to decimal form and stringified, then 
comma separated, then printed.";
DumpToConsole[matrix_] := Module[{i, string},
  For[i = 1, i <= Length[matrix], i++,
    Print[OutputForm@StringJoin[Riffle[ToString[#1//N] & /@ matrix[[i]], ", "]]];
  ]
];

(* =============================  CONSTANTS ================================== *)
(* The name of the feature we're prepending to all output from this script *)
FEATURETYPE = "markov"; 
BASICINFOCOLS = 5; (*The number of cols of non-voice information*)
COLSPERVOICE = 4; (* The number of columns that are needed per voice*)
(* W = whole note, d = dotted, H=half note, Q=quarter, and so on. 
   We only have rhythms that we've actually seen in renaissance pieces listed here. 
*)
rhythmAlphabet = {{"W", 1}, {"dH", .75}, {"tH", 0.666}, {"H", .5}, 
  {"dQ", .375}, {"tQ", 0.333}, {"Q", .25}, {"E", .125}};


(* ================================ LOGIC =================================== *)

flags = $ScriptCommandLine[[2]];
flagB = False; flagR = False; flagP = False;
If[StringMatchQ[flags, RegularExpression[".*b.*"]], flagB = True; ];
If[StringMatchQ[flags, RegularExpression[".*p.*"]], flagP = True; ];
If[StringMatchQ[flags, RegularExpression[".*r.*"]], flagR = True; ];

notes=Import[$ScriptCommandLine[[3]]];

If[notes == $Failed, Print["Invalid file: " <> $ScriptCommandLine[[3]]]; Exit[]];
notesheadless = notes[[3;;]];

Module[{i, col, rhythmfeatures, pitchfeatures}, 
Catch[
  For[i=1, i <= (Length[notesheadless[[1]]]-BASICINFOCOLS)/COLSPERVOICE, i++,
    col = 1+(i-1)*COLSPERVOICE+BASICINFOCOLS;
    
    If[flagR,
      rhythmfeatures= Select[MarkovWrapper[rhythmAlphabet[[;;,1]], 
        ExtractRhythm[notesheadless, rhythmAlphabet, col] 
      ], #[[2]] != 0&];

      If[rhythmfeatures != {},
        DumpToConsole@
          FeatureName[rhythmfeatures, FEATURETYPE, "voice"<>ToString[i]]
      ];
    ];

    If [flagP,
      pitchfeatures = Select[
        MarkovWrapper[Gather[#1][[;; , 1]] // Sort, #1] , #[[2]] != 0 &] &[
          ToString /@ Select[notesheadless, #1[[col]] > 0 &] [[;; , col]] 
        ];
      If[pitchfeatures != {},
        DumpToConsole[
          FeatureName[pitchfeatures, FEATURETYPE, "voice"<>ToString[i]]
        ];  
      ];    
    ];
  ];
, _, Function[{value, tag}, Print[{$ScriptCommandLine[[3]], value, tag}];] ];
];

Exit[] (* This should really not be needed, but it's here in case*)









