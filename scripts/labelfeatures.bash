#!/bin/bash

# File: labelfeatures.bash
# Author: Vishesh Gupta
# Created: 8 November 2013
# 
# 
# The purpose of this file is to take a labeled feature matrix 
# (i.e., a training or a test matrix), and output the correct corresponding labels.
# 
# For the purposes of this project we know that if the filename has a Joa in
# front, then we have a 1. Job= ?, since we don't know what that is supposed to 
# be, and anything else is a 0. 
# 

set -e
if [ $# -lt 1 ]; then
  echo "Usage: labelfeatures.bash featurematrix"
  exit 2
fi

printlabel() {
  line=$1
  if [[ $line =~ Joa ]]; then 
    echo 1
  elif [[ $line =~ Job ]]; then
    # we should be doing this, but since we can't calculate anything
    # that way,     
    # echo ?
    echo 0 # and then the test "error" will tell us which pieces are Josquin.
  else
    echo 0
  fi  
}

features=$(tail -n+2 $1)

echo "labels for $1"
OLD_IFS=$IFS
IFS=$'\n'
for line in $(echo "$features"); do 
  printlabel $line
done
IFS=OLD_IFS


