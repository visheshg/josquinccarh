[trainData, trainLabels, testData, testLabels] = randomize_test_data('../features/joa-ock_interval-and-duration.csv', '../features/joa-ock_interval-and-duration_labels.csv'); 


[errorpercent, error, output] = nb_test(trainData, trainLabels, testData, testLabels);
disp(errorpercent);