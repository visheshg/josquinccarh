#!/bin/bash

# File: totalpitchrange.bash
# Author: Vishesh Gupta
# Created: 27 October 2013
# 
# The idea behind this script is to process a whole bunch of NoteArray files and
# find all the unique base40 pitches. 
# 
# The script has two modes. 
# Running it as
# ./totalpitchrange.bash infile
# 
# will read the NoteArray file infile and find all the base40 columns and return
# the uniqed, sorted data in each base40 column.
# TODO: need to find base40 columns based on headers rather than hardcoded column values.
# 
# Running it as
# ./totalpitchrange.bash indir
# 
# will recursively search the entire directory (not just 1 level deep) and find
# all *.dat files, which are supposed to be notearray files. 
# TODO: allow for a custom extension to 'find'.
# 
# Then it will process ALL of them and return the uniqued, sorted pitches in
# ALL of the files it finds together. 


set -e # fail on the first failed command.

# the one file case
if [ "$#" -lt 1 ]; then
  echo "usage: totalpitchrange infile"
  echo "     : totalpitchrange indir"
  exit 0
fi

processonefile () { 
  infile=$1

  BASSCOL=6
  TENORCOL=10
  ALTOCOL=14
  SOPRANOCOL=18

  # collect the sorted, uniqed pitches from each voice.
  bass=`cut -f $BASSCOL $infile | egrep -v "(^-)|(^%)|(^0)|(1040)" | sort | uniq`
  alto=`cut -f $ALTOCOL $infile | egrep -v "(^-)|(^%)|(^0)|(1040)" | sort | uniq`
  tenor=`cut -f $TENORCOL $infile | egrep -v "(^-)|(^%)|(^0)|(1040)" | sort | uniq`
  soprano=`cut -f $SOPRANOCOL $infile | egrep -v "(^-)|(^%)|(^0)|(1040)" | sort | uniq`

  # print out the uniqed, sorted values from all of them
  echo -e "$bass""\n""$alto""\n""$tenor""\n""$soprano" | sort | uniq
}

infile=$1
if [ -f $infile ]; then
  processonefile $infile
elif [ -d $infile ]; then
  final=""
  for f in $(find $infile -name "*.dat"); do
    filevalues=$(processonefile $f)
    final=$(echo -e "$final""\n""$filevalues" | sort | uniq)
  done
  echo "$final"
fi













