#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* File: featureselection.m 
  Author: Vishesh Gupta
  Created: 11 December 2013
  
  Feature Selection is a process that assigns a score to each 
  column of a data matrix given its label matrix. 

  The metric being is the mutual information of the feature x and
  the class y. 
  
  NOTE: this system assumes you have a BINARY classification problem. 
  So y should only take on two different values. They should be 0 and 1, but
  this algorithm works with any two, I think. 
  
  Furthermore, this assumes the values of x are continuous between 0 and 1, and
  discretizes them assuming an Exponential distribution for the variables.

  After binning the varibles, we compute the various probabilites and 
  the overall metrics. 

  As you can see, this file is very specific to its application. In the future
  we could make it much more flexible, but for now, this is how it is.

  The metric is: Sum_y p(y) * Sum_x p(x|y) (ln p(x|y) - ln p(x))


  Usage: 
  featureselection.m -f featurefile labelfile
    Will select only the features that have a score of 1% or more.

  featureselection.m -r featurefile labelfile
    Will print out the complete rank of all of the features 
    (this is for post-analysis/giggles).

*)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 2, 
  Print["Usage: featureselection.m [-f|r] featurefile labelfile"]; Exit[]];


(* =============================== FUNCTIONS =============================== *)

Discretize[val_] := If[Chop[val] == 0, 0, Ceiling[val^(1/2)/.1] ];

MutualInformation[features_, featurelabels_, col_] := 
Module[{yprob, xprob, xyprob1, xyprob0, i},
  (*yprob = p(y), xprob = p(x), xyprob1 = p(x|y=1), xyprob0= p(x|y=0) *)

  (* Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &
  is a function that will generate the probability of each class
  that occurs. You should pass it Gather[somelist] and it will basically
  count 0s, 1s, 2s, etc. and then create ordered pairs like 
  {class, probability}. *) 
  
  yprob = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[featurelabels ] ];
  xprob = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[Discretize /@ features[[;; , col]] ] ];
  xyprob1 = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[
    (* We only want the ones where the feature labels say 1 *)
      Select[Transpose[{featurelabels}~
        Join~{Discretize /@ features[[;; , col]]}], #1[[1]] == 
        1 &][[;; , 2]]]
    ];
  xyprob0 = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[
    (* We only want the ones where the feature labels say 0 *)
      Select[Transpose[{featurelabels}~
        Join~{Discretize /@ features[[;; , col]]}], #1[[1]] == 
        0 &][[;; , 2]]]
    ];
  Return[  
    (* p(y==1) (p(x|y==1).p(x|y==1) + p(x|y==1).p(x) ) + 
       p(y==0) (p(x|y==0).p(x|y==0) + p(x|y==0).p(x) ) *)
    yprob[[2, 2]] * (xyprob1[[;; , 2]].Log@xyprob1[[;; , 2]] - 
                     #1[[1, ;; , 2]].Log@#1[[2, ;; , 2]] &[ 
       FrequencyPad[xyprob1, xprob] ]) +
    yprob[[1, 2]] * (xyprob0[[;; , 2]].Log@xyprob0[[;; , 2]] - 
                     #1[[1, ;; , 2]].Log@#1[[2, ;; , 2]] &[ 
       FrequencyPad[xyprob0, xprob] ])
  ];
];
FrequencyPad::usage="FrequencyPad[one, two] takes two Nx2 lists and does a
set intersection on them based on their first elements. Basically a workaround
for the fact that Mathematica doesn't allow comparators in set operations.
This function is really badly named. "
FrequencyPad[one_, two_] := Module[{hash1, hash2, intersect},
   hash1 = Hash[#1[[1]]] -> #1 & /@ one;
   hash2 = Hash[#1[[1]]] -> #1 & /@ two;
   intersect = hash1[[;; , 1]]~Intersection~hash2[[;; , 1]];
   Return[{intersect /. hash1, intersect /. hash2}];
   ];

DumpToConsole::usage="Writes out a matrix comma separated to stdout. 
Everything is first converted to decimal form and stringified, then 
comma separated, then printed.";
DumpToConsole[matrix_] := Module[{i, string},
  For[i = 1, i <= Length[matrix], i++,
    Print[OutputForm@StringJoin[Riffle[ToString[#1//N] & /@ matrix[[i]], ", "]]];
  ]
];

(* =============================  CONSTANTS ================================ *)
(* In order for a feature to be worth something it must contain at least 
1% of the information of the class y. *)
THRESHOLD = 0.01; 

(* ================================ LOGIC =================================== *)

features = Import[$ScriptCommandLine[[2]]];
If[features==$Failed, Print["Invalid features file: "<>$ScriptCommandLine[[2]]];
   Exit[]];

featurelabels = Import[$ScriptCommandLine[[3]]] // Flatten;
If[featurelabels == $Failed, 
  Print["Invalid labels file: " <> $ScriptCommandLine[[3]]];
  Exit[]
];

scores = Table[{col, Chop@N@ 
  MutualInformation[#1, #2, col]}, {col, 1, Length[#1[[1]] ], 1}] & [
    features[[2 ;;, 2 ;;]], featurelabels[[2;;]] ];

(* To see the list of scores with each feature name side by side. *)
ranklist = Sort[Flatten /@  Transpose[{scores}~ Join~{features[[1, 2 ;;]]}],
  #1[[2]] > #2[[2]] &];



(* This is the final output of the  analysis *)
selectedfeatures = features[[;; , {1}~Join~ 
  Select[Sort[scores, #1[[2]] > #2[[2]] &], #1[[2]] > THRESHOLD &][[;; , 1]] ]];

DumpToConsole[selectedfeatures];

Exit[] (* This should really not be needed, but it's here in case*)









