#!/bin/bash

# File: genallfeatures.bash
# Author: Vishesh Gupta
# 
# Purely a test script that generates all the features in featurescripts for 
# the file. 


dir=$@
scripts="./featurescripts"
for f in $(find $dir -name *.dat); do
  echo $f
  allfeatures=$(eval "$(sed '/^$/d' $scripts | sed "s/$/ \${f}/")")
  echo "$allfeatures"
done
