#!/bin/bash

# File: aggregatefeatures.bash
# Author: Vishesh Gupta
# Created: 3 November 2013
# 
# 
# Preamble: 
# --------------
# The purpose of this script is to take a whole bunch of scripts and
# a directory of data and generate all the features for all the files in a way
# that pads zeros for features that exist in one file but not in another.
# 
# The reason that this is a good feature is because most of our features are
# extracted histograms. We don't want to be responsible for padding zeros and 
# knowing what the unique members of every data file is. 
# 
# That's a lot of data to compute and store regularly since everything is so
# morphable so easily.
# 
# Instead, this system allows any feature generation scheme to output a distinct 
# label for each type of feature that it generates.
# 
# A simple example: 
# durationhistogram.m outputs labels such as
# durhist-[voice]-[base40 pitch]. 
# Since not every file has every base40 pitch, this script can output 
# whatever pitches can be found in the file.
# 
# Then this script takes all the various output across all the files and 
# computes a "master feature list".
# 
# A very simple example:
# 
# data file 1: 
# durhist-bass-116 0.22222
# 
# data file 2:
# durhist-bass-115 0.3114
# 
# Final output:
# feature file 1:
# durhist-bass-116 0.22222
# durhist-bass-115 0
# 
# feature file 2:
# durhist-bass-116 0
# durhist-bass-115 0.3114
# 
# 
# Aggregate final output:
# 
# filename durhist-bass-116 durhist-bass-115
# file 1    0.222222            0 
# file 2    0                   0.3114

# Usage
# 
# aggregatefeatures.bash scriptfile datadir

set -e 

if [ $# -lt 2 ]; then
  echo "Usage: aggregatefeatures.bash [-fm:] scriptfile datadir [datadir2...]"
  exit 1
fi

trap "killall background" SIGINT SIGTERM 

extractfeaturesfromonefile() {
  scriptfile=$1
  f=$2
  echo -e "$(eval "$(sed '/^$/d' $scriptfile | sed "s/$/ \${f}/")")"
}

extractfeaturenamesfromonefile() {
  features=$(extractfeaturesfromonefile $1 $2)
  echo -e "$features" | cut -d , -f 1
}

genmasterfeaturelist() {
  scriptfile=$1
  datadir=$2

  # first, generate the master feature list. 
  allfeatures=""
  for f in $(find $datadir -name *.dat); do
    features=$(extractfeaturenamesfromonefile $scriptfile $f)
    allfeatures=$(echo -e "$allfeatures""\n""$features" | sort -n | uniq)
  done

  # there's a glitch where we sometimes end up with a blank line, 
  # so remove that. 
  allfeatures=$(echo -e "$allfeatures" | sed '/^$/d' | sort -n)
  
  # the "return" value
  echo "$allfeatures"
}

# read args
args=`getopt fm: $*`
if [ $? != 0 ]; then
  echo "Usage: aggregatefeatures.bash [-fm:] scriptfile datadir"
  exit 2
fi

# default flag values go here
flag_featuresonly=false;

set -- $args
for i 
do 
  case "$i"
  in
    -f) flag_featuresonly=true; shift;;
    # make sure to clean the file before using it. 
    -m) allfeatures=$(cat $2 | sed '/^$/d'); shift; shift;; 
    --) shift; break;;
  esac
done

scriptfile=$1
shift
# datadir can have more than one directory.
datadir="$@"

# generate the master feature list
if [[ -z "$allfeatures" ]]; then
  allfeatures=$(genmasterfeaturelist "$scriptfile" "$datadir")
fi

# if we are only generating features then exit right here. 
if $flag_featuresonly; then
  echo "$allfeatures"
  exit 0
fi

# now that we've generated a valid master list of features, we need to run 
# each file again, but this time pad zeros for the features that weren't found
# and output a feature header bar with all the names. 
# 
# The features will be comma separated. 

#echo "total features:" $(echo "$allfeatures" | wc -l)

# print out header column with the names of the features. 
echo "filename," $(echo "$allfeatures" | paste -sd "," -)

for f in $(find $datadir -name *.dat); do
  fullfeatures=$(extractfeaturesfromonefile $scriptfile $f)  
  features=$(echo -e "$fullfeatures" | cut -d , -f 1)

  # contains a list of the features to pad 
  # this is a standard set complement/set subtraction operation
  # see http://www.catonmat.net/blog/set-operations-in-unix-shell/
  # 
  # I also clean up the empty lines at the end (sorta paranoid about this)
  padfeatures=$(echo -e "$features""\n""$features""\n""$allfeatures" | sort -n | uniq -u | sed '/^$/d')
  
  # terrible naming, I know, but paddedfeatures is padfeatures with 
  # a 0 added to every line. 
  paddedfeatures=$(echo -e "$padfeatures" | sed "s/$/ , 0/")

  # concatenate the two feature lists.
  finalfeatures=$(echo -e "$paddedfeatures""\n""$fullfeatures" | sort -n)

  # just print out the compressed final features
  echo "$f," $( echo "$finalfeatures" | cut -d , -f 2 | paste -sd "," -)
done



