#!/bin/bash

# File: cleannotearray.bash
# Author: Vishesh Gupta
# Created: 26 October 2013
# 
# NoteArray files come with a lot of cruft that relates to the actual generation of
# a proper score pdf from the NoteArray file.
# 
# This information doesn't matter in analyzing the music, so this script takes in a 
# notearray file and removes all of the cruft, leaving the basic column headers and 
# the actual note information. 
# 
# There are a couple modes for running this file:
# ./cleannotearray.bash infile 
# will clean the file and echo the result to stdout
# 
# ./cleannotearray.bash infile outfile
# will clean the file and echo the result to stdout and copy it to outfile
# if outfile is a directory, then the cleaned file will be copied (same name as
# infile) into that direcotry. 
# see: man tee
# 
# ./cleannotearray.bash indir outdir
# finds all notearray files that have the *.dat format and cleans them,
# saving the cleaned copies in outdir. 
# TODO: allow for custom extension, not just *.dat
# 
# ./cleannotearray.bash indir outdir suffix
# same as above, but the suffix string is appended to the name of each file 
# in indir and then copied to outdir.
# example: file: Jos0110.dat suffix: "_clean" outfile: Jos0110_clean.dat
# 


set -e #fail the program when the first command fails. 

if [ "$#" -lt 1 ]; then
  # if the input is a file, you can specify an outfile to send it to,
  # otherwise, it's returned on the command line 
  echo 'usage: cleannotearray.bash infile [outfile]'
  # if the input file is a directory, then it's possible to include a 
  # file suffix. if the file is indir/file.txt, then the program will
  # output outdir/file[filesuffix].txt. 
  echo '     : cleannotearray.bash indir outdir [filesuffix]'
  exit 1
fi

infile=$1
outfile="" #default is to print out the value on the command line
if [ "$#" -ge 2 ]; then
  outfile=$2
fi 
# when we have the input as a regular file,
# if the outfile is a directory, then append the name of the infile 
# to produce a valid file name for tee.
if [ -d $outfile -a -f $infile ]; then
  outfile=${outfile}${infile##*/}
fi

suffix="" #default is to not have a file suffix.
# read the suffix from the command line if it's there
if [ "$#" -ge 3 ]; then
  suffix=$3
fi

if [ -f $infile ]; then
  # here's the actual operation we want to do on this file.
  egrep "(idx)|(^\d)" $infile | tee $outfile

elif [ -d $infile ]; then
  
  if [ !-d $outfile ]; then
    echo "$outfile: not a valid directory"
    exit 1
  fi

  for f in $(find $infile -name '*.dat'); do
    # some complex string manipulation gives us the name of the 
    # output file which might contain
    relpath=${f##$infile} 
    nfile=$outfile${relpath%.*}$suffix"."${f##*.}
    echo $f
    echo $nfile
    egrep "(idx)|(^\d)" $f > $nfile 
  done
else
  echo "${infile}: No such file or directory"
  exit 1
fi
















