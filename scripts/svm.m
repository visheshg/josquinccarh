addpath('../../ps2Copy/liblinear-1.93/matlab');  % add LIBLINEAR to the path

dataFile = '../features/joa-rue_markov_pca.csv';
labelsFile = '../features/joa-rue_markov_labels.csv';
unknownFile = '../features/job_markov_pca.csv';
[model errPercent errMatrix] = cross_validation(@svmFn, dataFile, labelsFile, unknownFile);
errPercent
errMatrix