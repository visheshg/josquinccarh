function [phi1, phi0, phiy] = nb_train(trainMatrix, trainCategory)

numTrainDocs = size(trainMatrix, 1);
numTokens = size(trainMatrix, 2);

invTrainCategory = trainCategory==0;
phi1 = 1/(trainCategory*sum(trainMatrix,2)+numTokens) * (trainCategory*trainMatrix+1);
phi0 = 1/(invTrainCategory*sum(trainMatrix,2)+numTokens) * (invTrainCategory*trainMatrix + 1);
phiy = sum(trainCategory)/numel(trainCategory);

end