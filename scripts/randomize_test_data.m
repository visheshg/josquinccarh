function [trainData, trainLabels, testData, testLabels] = randomize_test_data(dataFile, labelsFile); 

data = csvread(dataFile, 0, 0);
labels = csvread(labelsFile, 1, 0);

data = [data labels];
shuffledData = data(randperm(size(data,1)),:);

numData = size(data, 1);
numTrain = round(numData * .7);
numTest = numData - numTrain;

trainData = shuffledData(1:numTrain, :);
testData = shuffledData(numTrain+1:numData, :);

trainLabels = trainData(:, size(trainData, 2))';
testLabels = testData(:, size(testData, 2))';
trainData(:, size(trainData, 2)) = [];
testData(:, size(testData, 2)) = [];

end

