function [model, unknownModel, unknownPosterior, error] = ...
    gdaFn(data, labels, shuffledData, shuffledLabels, start, stop, unknownData)
    
    m = size(shuffledData, 1);
    n = 50;
    trainSample = [shuffledData(1:start-1, 1:n); shuffledData(stop+1:m, 1:n)];
    trainLabelsSample = [shuffledLabels(1:start-1); shuffledLabels(stop+1:m)];
    [class,err,posterior] = classify(data(:, 1:n),trainSample,trainLabelsSample);
    
    % Label all data
    model = zeros(size(data, 1), 1);
    for i = 1 : length(model)
        model(i) = posterior(i, 1) < posterior(i, 2);
    end
    
    % Training error
    numErr = 0;
    for i = start : stop
        if model(i) ~= labels(i)
            numErr = numErr + 1;
        end
    end
    error = numErr / size(shuffledData, 1);
    
    [class, err, unknownPosterior] = classify(unknownData(:,1:n),trainSample, trainLabelsSample);
    unknownModel = zeros(size(unknownData, 1), 1);
    for i = 1 : length(unknownModel)
        unknownModel(i) = unknownPosterior(i, 1) < unknownPosterior(i, 2);
    end
end

