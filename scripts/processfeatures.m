#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* File: processfeatures.m 
  Author: Vishesh Gupta
  Created: 12 December 2013
  
  Does feature selection, PCA, and then feature selection again on a 
  list of features to output a final feature matrix where each column is a 
  combined PCA feature sorted by its effectiveness. 

  Usage:
  processfeatures.m trainfeatures trainlabels testfeatures

*)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 3, 
  Print["Usage: featureselection.m trainfile labelfile testfile"]; 
  Exit[]];


(* =============================== FUNCTIONS =============================== *)
(* Applying a slightly skewed discretization scheme to try and \
account for the fact that a majority of the data points will be < 0.1 \
*)
Discretize[val_] := If[Chop[val] == 0, 0, Ceiling[val^(1/2)/.1] ];

MutualInformation[features_, featurelabels_, col_] := 
Module[{yprob, xprob, xyprob1, xyprob0, i},
  (*yprob = p(y), xprob = p(x), xyprob1 = p(x|y=1), xyprob0= p(x|y=0) *)

  (* Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &
  is a function that will generate the probability of each class
  that occurs. You should pass it Gather[somelist] and it will basically
  count 0s, 1s, 2s, etc. and then create ordered pairs like 
  {class, probability}. *) 
  
  yprob = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[featurelabels ] ];
  xprob = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[Discretize /@ features[[;; , col]] ] ];
  xyprob1 = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[
    (* We only want the ones where the feature labels say 1 *)
      Select[Transpose[{featurelabels}~
        Join~{Discretize /@ features[[;; , col]]}], #1[[1]] == 
        1 &][[;; , 2]]]
    ];
  xyprob0 = Sort@Transpose@{#1[[;; , 1]], Normalize[Length /@ #1, Total]} &[
    Gather[
    (* We only want the ones where the feature labels say 0 *)
      Select[Transpose[{featurelabels}~
        Join~{Discretize /@ features[[;; , col]]}], #1[[1]] == 
        0 &][[;; , 2]]]
    ];
  Return[  
    (* p(y==1) (p(x|y==1).p(x|y==1) + p(x|y==1).p(x) ) + 
       p(y==0) (p(x|y==0).p(x|y==0) + p(x|y==0).p(x) ) *)
    yprob[[2, 2]] * (xyprob1[[;; , 2]].Log@xyprob1[[;; , 2]] - 
                     #1[[1, ;; , 2]].Log@#1[[2, ;; , 2]] &[ 
       ComparatorIntersection[xyprob1, xprob] ]) +
    yprob[[1, 2]] * (xyprob0[[;; , 2]].Log@xyprob0[[;; , 2]] - 
                     #1[[1, ;; , 2]].Log@#1[[2, ;; , 2]] &[ 
       ComparatorIntersection[xyprob0, xprob] ])
  ];
];
ComparatorIntersection::usage="ComparatorIntersection[one, two] 
takes two Nx2 lists and does a
set intersection on them based on their first elements. Basically a workaround
for the fact that Mathematica doesn't allow comparators in set operations. "
ComparatorIntersection[one_, two_] := Module[{hash1, hash2, intersect},
   hash1 = Hash[#1[[1]]] -> #1 & /@ one;
   hash2 = Hash[#1[[1]]] -> #1 & /@ two;
   intersect = hash1[[;; , 1]]~Intersection~hash2[[;; , 1]];
   Return[{intersect /. hash1, intersect /. hash2}];
];

(* =============================  CONSTANTS ================================ *)
(* In order for a feature to be worth something it must contain at least 
0.5% of the information of the class y. This number is entirely arbitrary 
and can change depending on what the situation demands. *)
THRESHOLD = 0.01; 

(* ================================ LOGIC =================================== *)

features = Import[$ScriptCommandLine[[2]]];
If[features==$Failed, 
  Print["Invalid trainfeatures file: "<>$ScriptCommandLine[[2]]];
  Exit[];
];

featurelabels = Import[$ScriptCommandLine[[3]]] // Flatten;
If[featurelabels == $Failed, 
  Print["Invalid labels file: " <> $ScriptCommandLine[[3]]];
  Exit[];
];

testfeatures = Import[$ScriptCommandLine[[4]]];
If[testfeatures == $Failed, 
  Print["Invalid testfeatures file: " <> $ScriptCommandLine[[4]]];
  Exit[];
];

Print["Completed importing all data. "];
Print["Computing the ranklist (feature selection). "];

scores = Table[{col+1, Chop@N@ 
  MutualInformation[#1, #2, col]}, {col, 1, Length[#1[[1]] ], 1}] & [
    features[[2 ;;, 2 ;;]], featurelabels[[2;;]] ];

(* To see the list of scores with each feature name side by side. *)
ranklist = Sort[Flatten /@  Transpose[{scores}~ Join~{features[[1, 2 ;;]]}],
  #1[[2]] > #2[[2]] &];



(* This is the final output of the  analysis *)
indexes = Select[Sort[scores, #1[[2]] > #2[[2]] &], 
  #1[[2]] > THRESHOLD &][[;; , 1]];
Print["Ranklist complete (feature selection). "];

selectedfeatures = features[[;; , {1}~Join~indexes]];

selectedtestfeatures = testfeatures[[;;, {1}~Join~indexes]];

Print["Feature selection complete. "];
Print["selected training features: ", Dimensions@selectedfeatures];
Print["selected testing features: ", Dimensions@selectedtestfeatures];
Print["Running PCA. (principal components analysis)"];

(* Now to run PCA on these selectedfeatures.  *)

allpca = PrincipalComponents[
  (selectedfeatures[[2 ;;, 2 ;;]]~Join~selectedtestfeatures[[2 ;;, 2 ;;]])
][[;; , ;; Length[selectedfeatures] + Length[selectedtestfeatures] -2 - 1 ]];

pcafeatures = Transpose[{selectedfeatures[[2 ;;, 1]]}~Join~
  Transpose@allpca[[;; Length[selectedfeatures] - 1]] ];

pcatestfeatures = Transpose[{testfeatures[[2 ;;, 1]]}~Join~
  Transpose@allpca[[Length[features];;]] ];

Print["PCA complete. "];

(* Finally, we want to order the pca features by the mutual information. *)

pcafeaturescores = Table[{col+1, Chop@N@ 
  MutualInformation[#1, #2, col]}, {col, 1, Length[#1[[1]] ], 1}] & [
    pcafeatures[[;;, 2 ;;]], featurelabels[[2;;]] ];

pcaindexes = Sort[pcafeaturescores, #1[[2]] > #2[[2]] &][[;; , 1]];

sortedpcafeatures = pcafeatures[[;;, {1}~Join~pcaindexes]];
sortedtestpcafeatures = pcatestfeatures[[;;, {1}~Join~pcaindexes]];

Print["pca feature selection complete."];

(* Now, we have the final features. There were a lot of intermediates
   generated in the process, and we should output all of them... *)

basefeaturefile = StringSplit[$ScriptCommandLine[[2]], "."][[1]];
basetestfeaturefile = StringSplit[$ScriptCommandLine[[4]], "."][[1]];

Print["basefeaturefile: ", StringSplit[$ScriptCommandLine[[2]], "."][[1]]];
Print["final dimensions of the features: "];
Print["trainfeatures: ", Dimensions@sortedpcafeatures];
Print["testfeatures: ", Dimensions@sortedtestpcafeatures];

Print["writing ranklist..."];
Export[basefeaturefile<>"_ranklist.csv", ranklist];
Print["writing selected training features..."];
Export[basefeaturefile<>"_selected.csv", selectedfeatures];
Print["writing selected test features..."];
Export[basetestfeaturefile<>"_selected.csv", selectedtestfeatures];
Print["writing pca training features..."];
Export[basefeaturefile<>"_pca.csv", pcafeatures];
Print["writing pca test features..."];
Export[basetestfeaturefile<>"_pca.csv", pcatestfeatures];
Print["writing sorted pca training features..."];
Export[basefeaturefile<>"_sortedpca.csv", sortedpcafeatures];
Print["writing sorted pca test features..."];
Export[basetestfeaturefile<>"_sortedpca.csv", sortedtestpcafeatures];

Exit[] (* This should really not be needed, but it's here in case*)


































