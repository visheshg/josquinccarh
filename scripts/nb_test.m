function [errorpercent, error, output] = nb_test(trainMatrix, trainLabels, testMatrix, testLabels)

numTestDocs = size(testMatrix, 1);
numTokens = size(testMatrix, 2);

[phi1, phi0, phiy] = nb_train(trainMatrix, trainLabels);

% Assume nb_train.m has just been executed, and all the parameters computed/needed
% by your classifier are in memory through that execution. You can also assume 
% that the columns in the test set are arranged in exactly the same way as for the
% training set (i.e., the j-th column represents the same token in the test data 
% matrix as in the original training data matrix).

% Write code below to classify each document in the test set (ie, each row
% in the current document word matrix) as 1 for SPAM and 0 for NON-SPAM.

% Construct the (numTestDocs x 1) vector 'output' such that the i-th entry 
% of this vector is the predicted class (1/0) for the i-th email (i-th row 
% in testMatrix) in the test set.
output = zeros(numTestDocs, 1);

%---------------
% YOUR CODE HERE
prob1 = (log(phiy) + testMatrix*log(phi1)');
prob0 = (log(1-phiy) + testMatrix*log(phi0)');
output = prob1 >= prob0;

%---------------

% Compute the error on the test set
error=0;
for i=1:numTestDocs
if (testLabels(i) ~= output(i))
    error=error+1;
    %disp(testLabels(i));
end
end

%Print out the classification error on the test set
errorpercent = error/numTestDocs;
end