dataFile = '../features/joa-rue_markov_pca.csv';
labelsFile = '../features/joa-rue_markov_labels.csv';
unknownFile = '../features/job_markov_pca.csv';

[model, errPercent, errMatrix, unknownModel, unknownPosteriors] = cross_validation(@gdaFn, dataFile, labelsFile, unknownFile);
errPercent
errMatrix
unknownModel = unknownModel';
unknownPosteriors = squeeze(unknownPosteriors);
combined = [unknownModel unknownPosteriors];