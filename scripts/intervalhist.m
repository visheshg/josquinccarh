#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(*
  File: intervalhist.m
  Author: Vishesh Gupta
  Created: 11 November 2013
  
  This script takes as input a cleaned notearray file (see cleannotearray.bash),
  and outputs features of the form:
  intervalhist-voice1_2-base40. 

  So the individual features correspond to the total duration of each interval 
  present between any two combinations of voices 
  (this can be a lot of features). 

  Just like durationhistogram.m, there is an optional -n flag that will normalize
  the data by dividing by the total duration of the piece. 

  The reason that it is optional is because some models, like logistic 
  regression, work better with normalized data (it makes more sense to
  compare the percentage of the various intervals being used than it does
  to compare the actual durations,
  since that would be skewed by how long each piece is).  
  
  In other cases, like Naive Bayes, it makes more sense _not_ to normalize, 
  due to the inbuilt Laplace smoothing present in the algorithm. 

  The choice is yours!  
 *)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 1, 
  Print["Usage: durationhistogram.m datafile"]; Exit[]];

(* =============================== FUNCTIONS =============================== *)

IntervalHistogram::usage="IntervalHistogram[notes] takes a matrix of the 
  notearray data that is clean (so if notes=Import[file], then notes[[3;;]])
  and generates a list of tuples of the form: {'n_m-base40', duration}, 
  where n and m are the numbers of 2 voices, counting up from the lowest 
  (bass=1), base40 is the interval (difference between the two pitches),
  and duration is the total duration (in beats) that that interval is
  played bewteen those two voices."
IntervalHistogram[notes_] := Module[{notesheadless, intervals, durintv},
  notesheadless=notes[[;;, 
    (* There are a lot of magic numbers in this row. 
    The idea is to get the index (1), abs duration (5), and then
    all the b40 columns, which are of the form 6+4i. *)
    {1, 5}~Join~Table[2 + 4 i, {i, 1, (Length[notes[[1]]] - 5)/4}] ]]; 
  
  intervals = {#1[[2]], 
    {ToString[#1[[1, 1]]] <> "_" <> ToString[#1[[2, 1]]], 
     If[Or[#1[[1, 2]] == 0, #1[[2, 2]] == 0], 0, Abs[#1[[2, 2]] - #1[[1, 2]]]]
    } & /@ 
    Subsets[Table[{i, Abs[#1[[i]]]}, {i, 1, Length[#1]}]& [#1[[3 ;;]]], {2}]}&/@ 
    notesheadless[[;; -2]];
   
  durintv = Transpose[ReplacePart[#1, 1 -> #2 - #1[[1]] ] &
            [Transpose[intervals], notesheadless[[2 ;;, 2]] ]];
   
  Sort[{#1[[1]]<>"-"<>ToString[#1[[2]]] &[#1[[1, 1]]], Total[#1[[;; , 2]]]} &/@ 
    Gather[
      Select[Flatten[
        Table[{#1, durintv[[i, 1]]} & /@ durintv[[i, 2]],{i, 1,Length[durintv]}]
      , 1], #1[[1, 2]] != 0 &] , 
    First[#1] == First[#2] &]]
   ];

ToPercentages::usage= "ToPercentages[data, col, total] divides the nth entry in 
each row of data by the value of total.";
ToPercentages[data_, col_, total_] := 
  ReplacePart[#1, col->#1[[col]]/total] & /@ data;

FeatureName::usage="FeatureName[hist, string] takes a Nx2 matrix 
that is intended to be a histogram, and prepends some string label to the 
first column in the order 'string-[[1]]', where [[1]] is the first 
part of every pair in the histogram.";
FeatureName[hist_, name_String] := ReplacePart[#1, 1->name<>#1[[1]]] & /@ hist;

DumpToConsole::usage="Writes out a matrix comma separated to stdout. 
Everything is first normalized to decimal form and stringified, then 
comma separated, then printed.";
DumpToConsole[matrix_] := Module[{i, string},
  For[i = 1, i <= Length[matrix], i++,
    Print[OutputForm@StringJoin[Riffle[ToString[#1//N]&/@ matrix[[i]], ", "]]];
  ]
];


(* =============================  CONSTANTS ==================================*)
FEATURETYPE="intervalhist"

(* ================================ LOGIC =================================== *)
If[$ScriptCommandLine[[2]] == "-n", 
  normalize=True; 
  notes=Import[$ScriptCommandLine[[3]]];
,
  normalize=False;
  notes = Import[$ScriptCommandLine[[2]]];
]

If[notes == $Failed, Print["Invalid file"]; Exit[]];
notesheadless = notes[[3;;]];

intervals=IntervalHistogram[notesheadless];
If[normalize, intervals=ToPercentages[intervals, 2, Last[notes][[5]] ] ];
DumpToConsole[FeatureName[intervals, FEATURETYPE<>"-voice-"]];


Exit[] (* Shouldn't be needed but here just in case *)












