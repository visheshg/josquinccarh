#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* File: durationhistogram.m 
   Author: Vishesh Gupta
   Created: 1 November 2013

   This file takes in the name of a cleaned notearray file 
   (see cleanontearray.bash) and outputs an array of tuples of the beat40
   pitch name of each note present in the file, as a frequency for each voice

   There are four total output arrays, each with a tuple of {pitch, percentage}
   containing the percentage of the piece that that voice uses that pitch
   in terms of audible duration. 


 *)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 1, 
  Print["Usage: durationhistogram.m datafile"]; Exit[]];


(* =============================== FUNCTIONS =============================== *)

DurationHistogram::usage = "DurationHistogram[notesheadless, col] 
Takes a notearray file and an index of the beat40 column for the voice to 
calculate the total of each unique pitch in the score.";
DurationHistogram[notes_, col_] := Sort[{#1[[1, 1]], Total[#1[[;; , 2]]]} & /@ 
  Gather[{#1[[col]], notes[[#1[[col + 3]], 5]] - notes[[#1[[col + 1]], 5]]} & /@ 
  Select[notes, #1[[col]] > 0 &], First[#1] == First[#2] &]];

ToPercentages::usage= "ToPercentages[data, col, total] divides the nth entry in 
each row of data by the value of total.";
ToPercentages[data_, col_, total_] := 
  ReplacePart[#1, col->#1[[col]]/total] & /@ data;

FeatureName::usage="FeatureName[hist, name, voice] takes a Nx2 matrix 
that is intended to be a histogram, and prepends some string label to the 
first column in the order 'name-voice-[[1]]', where [[1]] is the first 
part of every pair in the histogram.";
FeatureName[hist_, name_String, voice_String] :=
 Transpose[ReplacePart[Transpose[hist], 
   1 -> {name <>"-" <> voice <> "-" <> ToString[#1] & /@ Transpose[hist][[1]]}[[1]]
]];

DumpToConsole::usage="Writes out a matrix comma separated to stdout. 
Everything is first normalized to decimal form and stringified, then 
comma separated, then printed.";
DumpToConsole[matrix_] := Module[{i, string},
  For[i = 1, i <= Length[matrix], i++,
    Print[OutputForm@StringJoin[Riffle[ToString[#1//N] & /@ matrix[[i]], ", "]]];
  ]
];

(* =============================  CONSTANTS ================================== *)
(* The name of the feature we're prepending to all output from this script *)
FEATURETYPE = "durhist"; 
BASICINFOCOLS = 5; (*The number of cols that get eaten up by non-voice information*)
COLSPERVOICE = 4; (* The number of columns that are needed per voice*)

(* ================================ LOGIC =================================== *)
If[$ScriptCommandLine[[2]] == "-n", 
  normalize=True; 
  notes=Import[$ScriptCommandLine[[3]]];
,
  normalize=False;
  notes = Import[$ScriptCommandLine[[2]]];
]

If[notes == $Failed, Print["Invalid file"]; Exit[]];
notesheadless = notes[[3;;]];

Module[{i}, 
  For[i=1, i <= (Length[notesheadless[[1]]]-BASICINFOCOLS)/COLSPERVOICE, i++,
    voicefeatures=DurationHistogram[notesheadless, BASICINFOCOLS+1+(i-1)*COLSPERVOICE];
    If[normalize, 
      voicefeatures=ToPercentages[voicefeatures, 2, Last[notesheadless][[5]]];
    ];
    If[voicefeatures != {},
      DumpToConsole[FeatureName[voicefeatures, FEATURETYPE, "voice"<>ToString[i]]];
    ]
  ];
];

Exit[] (* This should really not be needed, but it's here in case*)









