
function [testOutput, unknownOutput, unknownPosterior, err] = ...
    svmFn(data, labels, shuffledData, shuffledLabels, start, stop, unknownData)

m = size(shuffledData, 1);
n = 50;
trainData = [shuffledData(1:start-1,1:n); shuffledData(stop+1:m,1:n)];
%testData = data(start:stop, :);
trainLabels = [shuffledLabels(1:start-1); shuffledLabels(stop+1:m)];
trainData = sparse(trainData);
shuffledData = sparse(shuffledData);
data = sparse(data);
labels = sparse(labels);

% Convert categories to +1 and -1
svm_trainCategory = 2 .* trainLabels - 1;
% Train SVM
model = train(svm_trainCategory, trainData);


% Convert categories to +1 and -1
svm_testCategory = 2 .* labels - 1;
% Test SVM
[testOutput, accuracy] = predict(svm_testCategory, data, model);

shuffled_svm_testCategory = 2 .* labels - 1;
[shuffledOutput, accuracy] = predict(shuffled_svm_testCategory, shuffledData, model);
% Convert categories back to 0 and +1
testOutput = (testOutput + 1) .* 0.5;
shuffledOutput = (shuffledOutput + 1) .* 0.5;

unknownLabels = zeros(size(unknownData, 1), 1);
unknownLabels = sparse(unknownLabels);
unknownData = sparse(unknownData);
[unknownOutput, accuracy] = predict(unknownLabels, unknownData, model);
unknownOutput = (unknownOutput + 1) .* 0.5;

% This isn't actually anything... You just need it for the general function
unknownPosterior = zeros(size(unknownData, 1), 2);

% Compute the error on the test set
numErr=0;
for i=start:stop
    if (shuffledLabels(i) ~= shuffledOutput(i))
        numErr=numErr+1;
    end
end
err = numErr / size(trainData, 1);
end

