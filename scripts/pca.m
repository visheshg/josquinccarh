#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* Runs the PCA algorithm on the input features file, and writes the resultant file
to the output.  *)

(* This function is different from most other DumpToConsoles since it takes 
into account the fact that some values can be less than 10^-6 and will 
show up as exponents. NumberForm forces the numbers to be decimals. *)
DumpToConsole::usage="Writes out a matrix comma separated to stdout. 
Everything is first converted to decimal form and stringified, then 
comma separated, then printed.";
DumpToConsole[matrix_] := Module[{i, string},
  For[i = 1, i <= Length[matrix], i++,
    Print[OutputForm@StringJoin[Riffle[
      ToString[NumberForm[#1 // N, Automatic, 
        ExponentFunction -> Function[{val}, Null] ]] & /@ matrix[[i]]
    , ", "]]];
  ]
];

features = Import[$ScriptCommandLine[[2]]][[2;;,2;;]];
If[features == $Failed, 
  Print["Invalid features file: " <> $ScriptCommandLine[[2]]];
  Exit[]
];


pca = PrincipalComponents[features];
DumpToConsole[pca[[;;, ;;Length[features]-1]] ];



