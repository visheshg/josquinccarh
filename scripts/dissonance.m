(* ::Package:: *)

#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(*  File: dissonance.m
	Author: Jacqueline Speiser
	Created: 11/27/13
*)

(*================================ ARGS checks ============================= *)
If[Length[$ScriptCommandLine] <= 1, 
  Print["Usage: durationhistogram.m datafile"]; Exit[]];


BASICINFOCOLS = 5; (*The number of cols that get eaten up by non-voice information*)
COLSPERVOICE = 4; (* The number of columns that are needed per voice*)

notes = Import["cleandata/Joa/Jos0301a_clean.dat"];
notesheadless = notes[[3;;]];
notesheadless

Module[{i},
	For[i = 1, i <=(Length[notesheadless[[1]]]-BASICINFOCOLS)/COLSPERVOICE, i++,
	
	];
];
