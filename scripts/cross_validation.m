function [bestModel totalErr errMatrix bestUnknownModel, bestUnknownPosterior] = ...
    cross_validation(algorithm, dataFile, labelsFile, unknownFile)
% Read in data
unknownData = csvread(unknownFile, 0, 1);
data = csvread(dataFile, 0, 1);
labels = csvread(labelsFile, 1, 0);

% Shuffle data
shuffledData = [data labels];
shuffledData = shuffledData(randperm(size(shuffledData,1)),:);
shuffledLabels = shuffledData(:, size(shuffledData, 2));
shuffledData(:, size(shuffledData, 2)) = [];

n = 50;
m = size(shuffledData, 1);
k = 10;
setSize = round(m / k);
start = 1;

models = zeros(k, size(shuffledData, 1));
unknownModels = zeros(k, size(unknownData, 1));
unknownPosteriors = zeros(k, size(unknownData, 1), 2);
modelErrs = zeros(k, 1);
for i = 1 : k
    % Define indexes of examples to set aside
    if i == k
        stop = m;
    else
        stop = start + setSize - 1;
    end
    
    % Run algorithm. Model returned is for unshuffled data.
    [model, unknownModel, unknownPosterior, err] = ...
        algorithm(data, labels, shuffledData, shuffledLabels, start, stop, unknownData);
    err
    modelErrs(i) = err;
    models(i, :) = model;
    unknownModels(i, :) = unknownModel;
    unknownPosteriors(i, :, :) = unknownPosterior;
    
    start = start + setSize;
end
[err, i] = min(modelErrs);
bestModel = zeros(m);
bestModel = models(i, :);
bestUnknownModel = unknownModels(i, :);
bestUnknownPosterior = unknownPosteriors(i, :, :);

errNum = 0;
countMatrix = zeros(2);
for i = 1 : size(data, 1)
    actual = labels(i);
    guess = bestModel(i);
    countMatrix(actual+1, guess+1) = countMatrix(actual+1, guess+1) + 1;
    if actual ~= guess
        errNum = errNum + 1;
    end
end
totalErr = errNum / size(shuffledData, 1);

numOne = sum(shuffledLabels);
numZero = length(shuffledLabels) - numOne;
errMatrix = countMatrix;
errMatrix(1, :) = errMatrix(1, :) / numZero;
errMatrix(2, :) = errMatrix(2, :) / numOne;

end

